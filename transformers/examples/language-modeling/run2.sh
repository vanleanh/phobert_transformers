export TRAIN_FILE=/home/dell/Desktop/transformers_fork_phoBert/transformers/examples/language-modeling/data_vinaphone/data_QnA_17thang9_tokenized.txt
export TEST_FILE=/home/dell/Desktop/transformers_fork_phoBert/transformers/examples/language-modeling/data_vinaphone/data_QnA_17thang9_tokenized.txt
export OUTPUT_DIR=/hdd/finetune/phoBert/large_qna/

python run_language_modeling.py \
    --output_dir=$OUTPUT_DIR \
    --model_type=roberta_large \
    --model_name_or_path=vinai/phobert-large \
    --do_train \
    --train_data_file=$TRAIN_FILE \
	--do_eval \
    --eval_data_file=$TRAIN_FILE \
	--eval_steps 5 \
    --overwrite_output_dir \
    --num_train_epochs 25 \
	--per_device_train_batch_size 8 \
	--logging_steps 5 \
    --line_by_line \
    --block_size 1024 \
    --save_steps 2000 \
    --mlm
